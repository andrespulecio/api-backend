# API-PRACTITIONER BACKEND #
### Introduction
This is the deliverable for the Practitioner-BackEnd, include an API, Data Base in MongoDB and deploy in Docker Hub.
### Requirements
This module requires the following tools:
* Java jdk-15 ( [https://www.oracle.com/java/technologies/javase/jdk15-archive-downloads.html](https://www.oracle.com/java/technologies/javase/jdk15-archive-downloads.html) )
* MongoDB ( [https://www.mongodb.com/es](https://www.mongodb.com/es) )
* Maven ( [https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi) )
### Execution
```bash
mvn spring-boot:run
```
You can view in [locahost:8081](http://locahost:8081/)
### Docker
You can view the image in:
* andrespulecio/api-practitioner ( [https://hub.docker.com/r/andrespulecio/api-practitioner/tags?page=1&ordering=last_updated](https://hub.docker.com/r/andrespulecio/api-practitioner/tags?page=1&ordering=last_updated) )


### Who do I talk to? ###

* Andres Felipe Pulecio Gomez
